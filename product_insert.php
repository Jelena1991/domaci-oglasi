<?php

  require("include/config.php");
  require("include/dbh.php");
  require("include/functions.php");

?>

<form action="realinsert.php" method="POST" enctype="multipart/form-data">
  <fieldset class="fieldset">
    <legend class="legend">Insert product</legend>
    <label>Name</label><br>
    <input type="text" name="name" value="" required="required"><br><br>
    <label>Image</label><br>
    <input type="file" name="file" required="required"><br><br>
    <label>Description</label><br>
    <textarea type="text" name="description"  value="" required="required"></textarea><br><br>
    <label>Price</label><br>
    <input type="text" name="price" value="" required="required"><br><br>
    <label>Date</label><br>
    <input type="date" name="date" value="" required="required" autocomplete="on"><br><br>
    <input type="submit" name="submitbutton" id="submitbutton" value="insert">
    <?php
    echo "<a href=\"index1.php\">Go back</a><br>";
    ?>
  </fieldset>
</form>